import "reflect-metadata";
import { Field, ID, InputType, ObjectType } from "type-graphql";
import { MaxLength, Length } from "class-validator";

@ObjectType()
export class User {
  @Field((type) => ID)
  id: string;

  @Field()
  name: string;

  @Field()
  age: number;
}

@InputType()
export class createUserInput {
  @Field()
  @MaxLength(30)
  name: string;

  @Field()
  age: number;
}
