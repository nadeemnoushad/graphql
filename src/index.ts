import "reflect-metadata";

import { Authorized, buildSchema } from "type-graphql";
import { ApolloServer } from "apollo-server";
import { UserResolver } from "./resolvers/User";
import { resolvers } from "./prisma/generated/type-graphql";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function bootstrap() {
  const schema = await buildSchema({
    resolvers,
    emitSchemaFile: true,
    validate: { forbidUnknownValues: false },
  });
  const context = () => {
    return {
      prisma,
    };
  };
  const server = new ApolloServer({ schema, context });

  server.listen().then(({ url }) => console.log(`Server running on ${url}`));
}

bootstrap();
