import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { UserCountPostArgs } from "./args/UserCountPostArgs";

@TypeGraphQL.ObjectType("UserCount", {})
export class UserCount {
  Post!: number;

  @TypeGraphQL.Field(_type => TypeGraphQL.Int, {
    name: "Post",
    nullable: false
  })
  getPost(@TypeGraphQL.Root() root: UserCount, @TypeGraphQL.Args() args: UserCountPostArgs): number {
    return root.Post;
  }
}
