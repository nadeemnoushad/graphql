import "reflect-metadata";
import { Resolver, Query, Mutation, Arg, Ctx } from "type-graphql";
import { createUserInput } from "../types/user";
import { User, UserCreateInput } from "../prisma/generated/type-graphql";
import { prisma } from "../prisma";

@Resolver()
export class UserResolver {
  private userCollection: User;

  @Query((returns) => [User])
  users() {
    return prisma.user.findMany();
  }

  @Mutation()
  async create(@Arg("data") { name, age, id }: UserCreateInput): Promise<User> {
    return await prisma.user.create({ data: { age, name, id } });
  }
}
